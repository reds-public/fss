/*****************************************************************
 * fss_gui_fli - interface between qtemu and QuestaSim           *
 *                                                               *
 * FSS project - REDS Institute, HEIG-VD, Yverdon-les-Bains (CH) *
 *  A. Dassatti, A. Convers, R. Rigamonti, X. Ruppen -- 12.2015  *
 *****************************************************************/

#ifndef FSS_GUI_FLI_H
#define FSS_GUI_FLI_H

#include "fss_qemu_common.h"

void set_switches_state(int state);

#endif /* FSS_GUI_FLI_H */
