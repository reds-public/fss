#ifndef CONFIG_H
#define CONFIG_H

#define TCP_PORT 4443

#define PERID_LED 		"led"
#define PERID_LCD 		"lcd"
#define PERID_SEVEN_SEG	"7seg"
#define PERID_BTN		"btn"

#define SET				"set"
#define UPDATE			"update"
#define CLEAR			"clear"

#endif // CONFIG_H
